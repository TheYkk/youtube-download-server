/* eslint-disable no-console */
// Require the framework and instantiate it
const fastify = require('fastify')({logger: false});
const {exec} = require('child_process');

// Declare a route
fastify.get('/download/:id', (request, reply) => {
  exec(
    `youtube-dl "${request.params.id}" -o "${request.params.id}.%(ext)s" `,
    (err, stdout, stderr) => {
      if (stderr){
        console.error(`exec error: ${err} `);
        return true;
      }
      // Regex for dublicate download
      const check = /\[download\] (.*) has already been downloaded/.exec(
        stdout,
      );

      // If file exists sen it name
      if (check[1]){
        reply.send({ok: check[1]});
        return true;
      }

      // Get file name and sen
      const dest = /(Destination:) (.*)/.exec(stdout);
      reply.send({ok: dest[2]});
    },
  );
});

// Run the server!
fastify.listen(3085, (err, address) => {
  if (err) throw err;
  fastify.log.info(`server listening on ${address}`);
  console.log(`server listening on ${address}`);
});
