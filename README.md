# YouTube Download Server

For install npm packages
```
yarn
```

For yotube dl use this shell command
 ```
sudo curl -L https://yt-dl.org/downloads/latest/youtube-dl -o /usr/local/bin/youtube-dl
sudo chmod a+rx /usr/local/bin/youtube-dl
  ```

Start server
```
yarn start
```

> server listening on http://127.0.0.1:3085

